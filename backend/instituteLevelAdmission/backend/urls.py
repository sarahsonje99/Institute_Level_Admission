from django.conf.urls import url
from backend.views import *
from django.views.decorators.csrf import csrf_exempt

app_name = 'backend'


urlpatterns = [
    url(r'^validate_student$', csrf_exempt(ValidateStudent.as_view())),  # login here itself
    url(r'^validate_staff$', csrf_exempt(ValidateStaff.as_view())),
    url(r'^send_personal_details$', csrf_exempt(AddPersonalDetails.as_view())),
    url(r'^send_academic_details$', csrf_exempt(AddAcademicDetails.as_view())),
    url(r'^send_documents$', csrf_exempt(AddDocuments.as_view())),
    url(r'^send_preferences$', csrf_exempt(AddPreferences.as_view())),
    url(r'^allocate_seats$', csrf_exempt(AllocateSeats.as_view())),
    url(r'^confirm_student_admission$', csrf_exempt(ConfirmAdmission.as_view())),
    url(r'^freeze_float_seat$', csrf_exempt(FreezeFloatSeat.as_view())),
    # url(r'^get_admission_details$', csrf_exempt(FreezeFloatSeat.as_view())),
    # url(r'^upload$', render_upload, name='upload'),
]