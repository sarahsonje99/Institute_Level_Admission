# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User

from django.db import models

# Create your models here.


class PersonalDetails(models.Model):
    name = models.CharField(max_length=1000)
    cet_rollno = models.CharField(max_length=10, unique=True)
    email = models.CharField(max_length=50)
    gender = models.CharField(max_length=100)
    dob = models.CharField(max_length=100, default=0.00)
    contact = models.CharField(max_length=50)
    address = models.CharField(max_length=500)
    caste = models.CharField(max_length=20)

    def __str__(self):
        return self.id


class AcademicDetails(models.Model):
    cet_maths = models.IntegerField(default=0)
    cet_physics = models.IntegerField(default=0)
    cet_chemistry = models.IntegerField(default=0)
    cet_rank = models.IntegerField(default=0)
    cet_total = models.IntegerField(default=0)
    score_twelfth = models.FloatField(default=0)
    score_tenth = models.FloatField(default=0)

    def __str__(self):
        return self.cet_rank


class Branch(models.Model):
    name = models.CharField(max_length=1000)
    vacant_seats = models.IntegerField(default=25)
    filled_seats = models.IntegerField(default=0)
    cutoff = models.IntegerField(default=0)

    def __str__(self):
        return self.id


class Preferences(models.Model):
    first_pref = models.ForeignKey(Branch, related_name='students_first_preference', default=None, null=True)
    second_pref = models.ForeignKey(Branch, related_name='students_second_preference', default=None, null=True)
    third_pref = models.ForeignKey(Branch, related_name='students_third_preference', default=None, null=True)
    fourth_pref = models.ForeignKey(Branch, related_name='students_fourth_preference', default=None, null=True)

    def __str__(self):
        return self.id


class Round(models.Model):
    round_number = models.IntegerField(default=0)
    total_applicants = models.IntegerField(default=0)

    def __str__(self):
        return self.round_number


class Documents(models.Model):
    cet_application = models.BooleanField(default=False)
    twelfth_marksheet = models.BooleanField(default=False)
    tenth_marksheet = models.BooleanField(default=False)
    domicile = models.BooleanField(default=False)
    migration_certificate = models.BooleanField(default=False)


class Student(models.Model):
    arc_no = models.CharField(max_length=10, unique=True)
    p_details = models.ForeignKey(PersonalDetails, default=None, null=True)
    acad_details = models.ForeignKey(AcademicDetails, default=None, null=True)
    prefs = models.ForeignKey(Preferences, default=None, null=True)
    docs = models.ForeignKey(Documents, default=None, null=True)
    rank = models.IntegerField(default=0)
    consider_next = models.BooleanField(default=False)
    final_branch = models.ForeignKey(Branch, default=None, null=True)
    doc_verified = models.BooleanField(default=False)

    def __str__(self):
        return self.arc_no


class Allocation(models.Model):
    round_no = models.ForeignKey(Round, default=None)
    student = models.ForeignKey(Student, default=None)
    round_rank = models.IntegerField(default=0)
    allocated_branch = models.ForeignKey(Branch, default=None)
    admission_confirmed = models.BooleanField(default=False)
    float_freeze_seat = models.BooleanField(default=True)

    def __str__(self):
        return self.id
