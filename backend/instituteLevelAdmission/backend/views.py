# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# from django.shortcuts import render, redirect
from django.contrib.auth import login, logout, authenticate
from rest_framework.views import APIView
from backend.models import Student, PersonalDetails, AcademicDetails, Documents, Preferences, Branch, Round, Allocation
from rest_framework.parsers import MultiPartParser, JSONParser
from django.db.models import Max

from django.http import HttpResponse
from rest_framework.response import Response
# from django.core.serializers.json import DjangoJSONEncoder
import json

# Create your views here.


class ValidateStaff(APIView):

    # parser_classes = (MultiPartParser,)

    def post(self, request):
        try:
            print(request.body)
            req = json.loads(request.body)
            user = authenticate(username=req['username'], password=req['password'])
            if user.is_authenticated():
                students_dict = dict()
                for student in Student.objects.filter():
                    if student.p_details is not None:

                        personal_dict = dict()
                        # p_details = PersonalDetails.objects.filter(id = student)
                        if student.p_details is not None:
                            personal_dict['full_name'] = student.p_details.name
                            personal_dict['cet_rollno'] = student.p_details.cet_rollno
                            personal_dict['email'] = student.p_details.email
                            personal_dict['dob'] = student.p_details.dob
                            personal_dict['contact'] = student.p_details.contact
                            personal_dict['address'] = student.p_details.address

                        academic_dict = dict()
                        if student.acad_details is not None:
                            academic_dict['cet_maths'] = student.acad_details.cet_maths
                            academic_dict['cet_physics'] = student.acad_details.cet_physics
                            academic_dict['cet_chemistry'] = student.acad_details.cet_chemistry
                            academic_dict['cet_rank'] = student.acad_details.cet_rank
                            academic_dict['cet_total'] = student.acad_details.cet_total
                            academic_dict['score_twelfth'] = student.acad_details.score_twelfth
                            academic_dict['score_tenth'] = student.acad_details.score_tenth

                        pref_dict = dict()
                        if student.prefs is not None:
                            pref_dict['first_pref'] = student.prefs.first_pref.id
                            pref_dict['second_pref'] = student.prefs.second_pref.id
                            pref_dict['third_pref'] = student.prefs.third_pref.id
                            pref_dict['fourth_pref'] = student.prefs.fourth_pref.id

                        docs_dict = dict()
                        if student.docs is not None:
                            docs_dict['cet_application'] = student.docs.cet_application
                            docs_dict['twelfth_marksheet'] = student.docs.twelfth_marksheet
                            docs_dict['tenth_marksheet'] = student.docs.tenth_marksheet
                            docs_dict['domicile'] = student.docs.domicile
                            docs_dict['migration_certificate'] = student.docs.migration_certificate

                        rank = student.rank
                        docs_verified = student.doc_verified
                        final_branch = student.final_branch

                        alloc_dict = dict()
                        count = 1

                        for rnd in Round.objects.filter().order_by('round_number'):
                            if rnd.round_number is not 0:
                                print rnd.round_number
                                rn = rnd.round_number
                                alloc = Allocation.objects.filter(round_no=rnd).filter(student=student).first()
                                round_dict = dict()
                                round_dict['round_number'] = rn
                                round_dict['round_rank'] = alloc.round_rank
                                round_dict['total_applicants'] = rnd.total_applicants
                                print "balle balle"
                                round_dict['allocated_branch'] = alloc.allocated_branch.id
                                round_dict['admission_confirmed'] = alloc.admission_confirmed
                                round_dict['float_freeze_seat'] = alloc.float_freeze_seat
                                print "balle balle"
                                print round_dict
                                alloc_dict[(str)(count)] = round_dict
                                count = count + 1
                        personal_json = json.dumps(personal_dict)
                        acad_json = json.dumps(academic_dict)
                        pref_json = json.dumps(pref_dict)
                        docs_json = json.dumps(docs_dict)
                        alloc_json = json.dumps(alloc_dict)

                        students_dict[student.arc_no] = { "Allocation": alloc_json, "Documents": docs_json,
                        "Personal":personal_json, "Academic":acad_json, "Preferences":pref_json,
                        "rank": rank, "docs_verfied": docs_verified, "final_branch": final_branch}

                students_json = json.dumps(students_dict)
                return Response({"authorization": "success", "Students": students_json})
            else:
                return Response({"authorization": "failed"})
        except Exception as e:
            print e
            return Response({"authorization": "failed"})

    def get(self, request):
        return HttpResponse(status=403)


class ValidateStudent(APIView):

    # parser_classes = (JSONParser,)

    def post(self, request):
        try:
            print(request.body)
            req = json.loads(request.body)
            print req
            student = Student.objects.filter(arc_no=req['arc_no']).first()
            if student is not None:
                personal_dict = dict()
                # p_details = PersonalDetails.objects.filter(id = student)
                if student.p_details is not None:
                    personal_dict['full_name'] = student.p_details.name
                    personal_dict['cet_rollno'] = student.p_details.cet_rollno
                    personal_dict['email'] = student.p_details.email
                    personal_dict['dob'] = student.p_details.dob
                    personal_dict['contact'] = student.p_details.contact
                    personal_dict['address'] = student.p_details.address

                academic_dict = dict()
                if student.acad_details is not None:
                    academic_dict['cet_maths'] = student.acad_details.cet_maths
                    academic_dict['cet_physics'] = student.acad_details.cet_physics
                    academic_dict['cet_chemistry'] = student.acad_details.cet_chemistry
                    academic_dict['cet_rank'] = student.acad_details.cet_rank
                    academic_dict['cet_total'] = student.acad_details.cet_total
                    academic_dict['score_twelfth'] = student.acad_details.score_twelfth
                    academic_dict['score_tenth'] = student.acad_details.score_tenth
                
                print "Kayy"
                pref_dict = dict()
                if student.prefs is not None:
                    pref_dict['first_pref'] = student.prefs.first_pref.id
                    pref_dict['second_pref'] = student.prefs.second_pref.id
                    pref_dict['third_pref'] = student.prefs.third_pref.id
                    pref_dict['fourth_pref'] = student.prefs.fourth_pref.id

                docs_dict = dict()
                if student.docs is not None:
                    docs_dict['cet_application'] = student.docs.cet_application
                    docs_dict['twelfth_marksheet'] = student.docs.twelfth_marksheet
                    docs_dict['tenth_marksheet'] = student.docs.tenth_marksheet
                    docs_dict['domicile'] = student.docs.domicile
                    docs_dict['migration_certificate'] = student.docs.migration_certificate

                rank = student.rank
                docs_verified = student.doc_verified
                final_branch = student.final_branch

                alloc_dict = dict()
                count = 1
                print "mumma mia"

            
                for rnd in Round.objects.filter().order_by('round_number'):
                    if rnd.round_number is not 0:
                        print rnd.round_number
                        rn = rnd.round_number
                        alloc = Allocation.objects.filter(round_no=rnd).filter(student=student).first()
                        round_dict = dict()
                        round_dict['round_number'] = rn
                        round_dict['round_rank'] = alloc.round_rank
                        round_dict['total_applicants'] = rnd.total_applicants
                        print "balle balle"
                        round_dict['allocated_branch'] = alloc.allocated_branch.id
                        round_dict['admission_confirmed'] = alloc.admission_confirmed
                        round_dict['float_freeze_seat'] = alloc.float_freeze_seat
                        print "balle balle"
                        print round_dict
                        alloc_dict[(str)(count)] = round_dict
                        count = count+1
                print "balle balle"
                personal_json = json.dumps(personal_dict)
                print "should work"
                acad_json = json.dumps(academic_dict)
                print "should work"
                pref_json = json.dumps(pref_dict)
                print "should work"
                docs_json = json.dumps(docs_dict)
                alloc_json = json.dumps(alloc_dict)

                print "should work"
                return Response({"authorization": "success", "Allocation": alloc_json, "Documents": docs_json, "Personal":personal_json,
                                 "Academic":acad_json, "Preferences":pref_json, "rank": rank, "docs_verfied": docs_verified, "final_branch": final_branch})
            else:
                return Response({"authorization": "failed"})
        except Exception as e:
            print e
            return Response({"authorization": "failed"})

    def get(self, request):
        return HttpResponse(status=403)


class AddPersonalDetails(APIView):
    # parser_classes = (MultiPartParser,)

    def post(self, request):
        try:
            print(request.body)
            req = json.loads(request.body)
            student = Student.objects.filter(arc_no=req['arc_no']).first()
            if student is not None:
                if student.p_details is None:
                    pd = PersonalDetails()
                    pd.name = req['full_name']
                    pd.cet_rollno = req['cet_rollno']
                    pd.email = req['email']
                    pd.gender = req['gender']
                    pd.dob = req['dob']
                    pd.contact = req['contact']
                    pd.address = req['address']
                    pd.caste = req['caste']
                    pd.save()
                    student.p_details = pd
                    student.doc_verified = False
                    student.fees_paid = False
                    student.consider_next = False
                    student.save()
                    return Response({"authorization": "success"})
                else:
                    return Response({"authorization":"already exists"})
            else:
                return Response({"authorization": "failed"})
        except Exception as e:
            print e
            return Response({"authorization": "failed"})

    def get(self, request):
        return HttpResponse(status=403)

class AddAcademicDetails(APIView):
    parser_classes = (MultiPartParser,)

    def post(self, request):
        try:
            print(request.body)
            req = json.loads(request.body)
            student = Student.objects.filter(arc_no=req['arc_no']).first()
            if student is not None:
                if student.acad_details is None:
                    ad = AcademicDetails()
                    ad.cet_maths = req['cet_maths']
                    ad.cet_physics = req['cet_physics']
                    ad.cet_chemistry = req['cet_chemistry']
                    ad.cet_rank = req['cet_rank']
                    ad.cet_total = req['cet_total']
                    ad.score_twelfth = req['score_twelfth']
                    ad.score_tenth = req['score_tenth']
                    ad.save()
                    student.acad_details = ad
                    student.doc_verified = False
                    student.fees_paid = False
                    student.consider_next = False
                    student.save()
                    return Response({"authorization": "success"})
                else:
                    return Response({"authorization":"already exists"})
            else:
                return Response({"authorization": "failed"})
        except Exception as e:
            print e
            return Response({"authorization": "failed"})

    def get(self, request):
        return HttpResponse(status=403)


class AddDocuments(APIView):
    parser_classes = (MultiPartParser,)

    def post(self, request):
        try:
            print(request.body)
            req = json.loads(request.body)
            student = Student.objects.filter(arc_no=req['arc_no']).first()
            if student is not None:
                if student.docs is None:
                    docs = Documents()
                    docs.cet_application = req['cet_application']
                    docs.twelfth_marksheet = req['twelfth_marksheet']
                    docs.tenth_marksheet = req['tenth_marksheet']
                    docs.domicile = req['domicile']
                    docs.migration_certificate = req['migration_certificate']
                    docs.save()
                    student.docs = docs
                    student.save()
                    return Response({"authorization": "success"})
                else:
                    return Response({"authorization":"already exists"})
            else:
                return Response({"authorization": "failed"})
        except Exception as e:
            print e
            return Response({"authorization": "failed"})

    def get(self, request):
        return HttpResponse(status=403)


class AddPreferences(APIView):
    parser_classes = (MultiPartParser,)

    def post(self, request):
        try:
            print(request.body)
            req = json.loads(request.body)
            student = Student.objects.filter(arc_no=req['arc_no']).first()
            if student is not None:
                if student.prefs is None:
                    prefs = Preferences()
                    br = Branch.objects.filter(id=(req['first_pref'])).first()
                    print "hey"
                    prefs.first_pref = br
                    print "Hey1"
                    br = Branch.objects.filter(id=req['second_pref']).first()
                    prefs.second_pref = br
                    br = Branch.objects.filter(id=req['third_pref']).first()
                    prefs.third_pref = br
                    br = Branch.objects.filter(id=req['fourth_pref']).first()
                    prefs.fourth_pref = br
                    prefs.save()
                    student.prefs = prefs
                    student.consider_next = True
                    student.save()
                    count = 1
                    for std in Student.objects.filter().order_by('acad_details'):
                        std.rank = count
                        print
                        std.rank
                        std.save()
                        count = count + 1
                    return Response({"authorization": "success"})
                else:
                    return Response({"authorization":"already exists"})
            else:
                return Response({"authorization": "failed"})
        except Exception as e:
            print e
            return Response({"authorization": "failed"})

    def get(self, request):
        return HttpResponse(status=403)


class AllocateSeats(APIView):

    # parser_classes = (MultiPartParser,)

    def post(self, request):
        try:
            print(request.body)
            req = json.loads(request.body)
            user = authenticate(username=req['username'], password=req['password'])
            if user.is_authenticated():
                last_round = Round.objects.all().aggregate(Max('round_number'))
                last_round_number = last_round['round_number__max']

                print last_round_number

                br1 = Branch.objects.filter(name='Computer Engineering').first()
                br1.vacant_seats = req['cs']
                br1.save()
                br2 = Branch.objects.filter(name='Information Technology').first()
                br2.vacant_seats = req['it']
                br2.save()
                br3 = Branch.objects.filter(name='Electronics and Telecommunication').first()
                br3.vacant_seats = req['extc']
                br3.save()
                br4 = Branch.objects.filter(name='Electronics').first()
                br4.vacant_seats = req['etrx']
                br4.save()
                rnd = Round()
                rnd.round_number = last_round_number+1
                rnd.save()
                print 'hey'


                if last_round_number is 0:
                    print "Heyyy"
                    for stud in Student.objects.filter():
                        if stud.rank is not 0:
                            print "Shloka ko kuch nahi aata"
                            stud.consider_next = True
                            stud.save()
                else:
                    for seat in Allocation.objects.filter(round_no=last_round_number).filter(
                            admission_confirmed=True).filter(float_freeze_seat=True):
                        seat.student.consider_next = True

                    for seat in Allocation.objects.filter(round_no=last_round_number).filter(allocated_branch=None):
                        seat.student.consider_next = True

                print 'hey1'
                counter=1
                for student in Student.objects.filter(consider_next=True).order_by('rank'):
                    if student.prefs.first_pref.vacant_seats>0:
                        branch = Branch.objects.filter(id=student.prefs.first_pref.id).first()
                        print "waasup"

                    elif student.prefs.second_pref.vacant_seats>0:
                        branch = Branch.objects.filter(id=student.prefs.second_pref.id).first()

                    elif student.prefs.third_pref.vacant_seats>0:
                        branch = Branch.objects.filter(id=student.prefs.third_pref.id).first()

                    elif student.prefs.fourth_pref.vacant_seats>0:
                        branch = Branch.objects.filter(id=student.prefs.fourth_pref.id).first()

                    else:
                        continue
                    print "ssup"
                    branch.vacant_seats = branch.vacant_seats - 1
                    branch.filled_seats = branch.filled_seats + 1
                    branch.cutoff = student.acad_details.cet_total
                    branch.save()
                    alloc = Allocation()
                    alloc.round_no = rnd
                    alloc.student = student
                    alloc.round_rank = counter
                    counter = counter + 1
                    alloc.allocated_branch = branch
                    alloc.save()
                    print student
                    student.save()
                rnd.total_applicants =  counter-1
                rnd.save()
                for std in Student.objects.filter():
                    std.consider_next = False
                    std.save()
                return Response({"authorization": "success"})
            else:
                return Response({"authorization": "failed"})
        except Exception as e:
            print e
            return Response({"authorization": "failed"})

    def get(self, request):
        return HttpResponse(status=403)


class ConfirmAdmission(APIView):

    # parser_classes = (MultiPartParser,)

    def post(self, request):
        try:
            print(request.body)
            req = json.loads(request.body)
            user = authenticate(username=req['username'], password=req['password'])
            if user.is_authenticated():
                last_round = Round.objects.all().aggregate(Max('round_number'))
                last_round_number = last_round['round_number__max']
                print last_round_number

                student = Student.objects.filter(arc_no=req['arc_no']).first()
                l_round = Round.objects.filter(round_number=last_round_number)
                alloc = Allocation.objects.filter(round_no=l_round).filter(student=student).first()
                alloc.admission_confirmed = True
                student.doc_verified = True
                alloc.save()
                student.save()
                return Response({"authorization": "success"})
            else:
                return Response({"authorization": "failed"})
        except Exception as e:
            print e
            return Response({"authorization": "failed"})

    def get(self, request):
        return HttpResponse(status=403)


class FreezeFloatSeat(APIView):
    # parser_classes = (MultiPartParser,)

    def post(self, request):
        try:
            print(request.body)
            req = json.loads(request.body)
            student = Student.objects.filter(arc_no=req['arc_no']).first()
            if student is not None:
                if student.admission_confirmed is True:
                    freeze = req['freeze']
                    if freeze is 0:
                        # float seat
                        last_round = Round.objects.all().aggregate(Max('round_number'))
                        last_round_number = last_round['round_number__max']
                        alloc = Allocation.objects.filter(round_no=last_round_number).filter(student=student).first()
                        alloc.float_freeze_seat = True
                        alloc.save()
                        student.final_branch = alloc.allocated_branch

                    student.save()
                    return Response({"authorization": "success"})
                else:
                    return Response({"authorization":"fees not paid"})
            else:
                return Response({"authorization": "failed"})
        except Exception as e:
            print e
            return Response({"authorization": "failed"})

    def get(self, request):
        return HttpResponse(status=403)

#todo confirm addmision freeze.floatf