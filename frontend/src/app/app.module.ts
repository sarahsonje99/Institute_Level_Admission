import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { LoginCompComponent } from './login-comp/login-comp.component';
import { MainComponent } from './main/main.component';
import { FormsModule } from '@angular/forms';
import { AppRouting } from './app.routing';
import { StudentDashComponent } from './main/student-dash/student-dash.component';
import { AdminDashComponent } from './main/admin-dash/admin-dash.component';
import { StudentService } from './shared/student.service';
import { RegFormComponent } from './main/student-dash/reg-form/reg-form.component';
import { HttpClientModule } from '@angular/common/http';
import { AdminService } from './shared/admin.service';
import { PaymentComponent } from './main/student-dash/reg-form/payment/payment.component';
import { ViewAllocComponent } from './main/admin-dash/view-alloc/view-alloc.component';
import { NewRoundComponent } from './main/admin-dash/new-round/new-round.component';
import { ConfirmAdComponent } from './main/admin-dash/confirm-ad/confirm-ad.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginCompComponent,
    MainComponent,
    StudentDashComponent,
    AdminDashComponent,
    RegFormComponent,
    PaymentComponent,
    ViewAllocComponent,
    NewRoundComponent,
    ConfirmAdComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRouting,
    HttpClientModule
  ],
  providers: [StudentService, AdminService],
  bootstrap: [AppComponent]
})
export class AppModule { }
