import { Routes, RouterModule } from "@angular/router";
import { NgModule, Component } from "@angular/core";

import { MainComponent } from "./main/main.component";
import { LoginCompComponent } from "./login-comp/login-comp.component";
import { StudentDashComponent } from "./main/student-dash/student-dash.component";
import { AdminDashComponent } from "./main/admin-dash/admin-dash.component";
import { RegFormComponent } from "./main/student-dash/reg-form/reg-form.component";
import { ViewAllocComponent } from "./main/admin-dash/view-alloc/view-alloc.component";
import { NewRoundComponent } from "./main/admin-dash/new-round/new-round.component";

const appRoutes: Routes = [
  { path: "", component: LoginCompComponent },
  {
    path: "main",
    component: MainComponent,
    children: [
      { path: "student_dash/:s_arc", component: StudentDashComponent },
      {
        path: "student_dash/:s_arc/registration_form",
        component: RegFormComponent
      },

      {
        path: "admin_dash/:admin_id",
        component: AdminDashComponent,
        children: [
          { path: "view_Alloc", component: ViewAllocComponent },
          { path: "new_Round", component: NewRoundComponent }
        ]
      }

      // { path: "", redirectTo: "student_dash/:s_arc", pathMatch: "full" }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRouting {}
