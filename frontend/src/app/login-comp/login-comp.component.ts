import { Component, OnInit, Input, OnChanges } from "@angular/core";
import { NgForm } from "@angular/forms";
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";
// import 'rxjs';
// import { Observable } from "rxjs/Rx";
// import 'rxjs/add/operator/map';
// import 'rxjs/Rx';

import { map } from "rxjs/operators";

import { Student } from "../models/student.model";
import { Admin } from "../models/admin.model";
import { StudentService } from "../shared/student.service";
import { AdminService } from "../shared/admin.service";

@Component({
  selector: "app-login-comp",
  templateUrl: "./login-comp.component.html",
  styleUrls: ["./login-comp.component.css"]
})
export class LoginCompComponent implements OnInit {
  studentLogin: boolean = true;
  student: Student;
  admin: Admin;
  Resp: any;
  Try: any;
  login_message: string = "Login as Staff";

  constructor(
    private router: Router,
    private http: HttpClient,
    private studserv: StudentService,
    private adminserv: AdminService
  ) {
    this.student = new Student();
    this.admin = new Admin();

    this.studserv.getStudentFromARC(this.student.arc_number).subscribe(res => {
      console.log(res.authorization);
      this.Resp = res;
      console.log(this.Resp);
    });
  }

  ngOnInit() {
    this.studserv.getStudentFromARC(this.student.arc_number).subscribe(res => {
      console.log(res.authorization);
      this.Resp = res;
      console.log(this.Resp);
    });
  }

  toggleLogin() {
    this.studentLogin = !this.studentLogin;
    if (this.studentLogin) {
      this.login_message = "Login as Staff";
      console.log(this.login_message);
    } else {
      this.login_message = "Login as Student";
      console.log(this.login_message);
    }
  }

  login(loginForm: NgForm) {
    console.log(loginForm);
    this.student.arc_number = loginForm.value.student_arc;
    this.student.cet_number = loginForm.value.student_cet;
    this.admin.username = loginForm.value.staff_username;
    this.admin.password = loginForm.value.staff_password;

    if (this.studentLogin) {
      this.Resp = this.studserv
        .getStudentFromARC(this.student.arc_number)
        .subscribe((res: any) => {
          // let jsonresp = JSON.parse(res.text());
          console.log(res.authorization);
          if (res.authorization === "success") {
            this.router.navigate([
              "main/student_dash/",
              this.student.arc_number
            ]);
          } else {
            alert("Invalide ARC Number and CET Roll Number");
            //   //fill admin username here
            //   this.router.navigate(['main/admin_dash', this.admin.username]);
          }
        });
    } else {
      this.Resp = this.adminserv
        .getAdmin(this.admin.username, this.admin.password)
        .subscribe(res => {
          console.log(res.authorization);
          console.log(res);
          //res.students GIVES ALL STUDENTS PROFILES - use for viewing registerd students
          if (res.authorization === "success") {
            this.adminserv.setAdmin(this.admin);

            this.router.navigate(["main/admin_dash", this.admin.username]);
          } else {
            alert("Invalid Login credentials. Please try again");
          }
        });
      // this.Resp = this.adminserv.getAdmin(this.admin.username, this.admin.password);
      // console.log(this.Resp);
      // if(this.Resp.authorization ==="success")
      // this.router.navigate(['main/admin_dash', this.admin.username]);
    }
  }
}
