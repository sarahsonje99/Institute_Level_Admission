import { Injectable } from "@angular/core";
import { Student } from "../models/student.model";
import { Students } from "../data-mock/student.data";
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import 'rxjs/add/operator/map';


// import { of } from 'rxjs/observable/of';
import 'rxjs/observable/of';
import { NgForm } from "@angular/forms";
// import { Observable } from 'rxjs';
// import { of } from 'rxjs/Observable/of';
// import 'rxjs/Rx';

@Injectable()
export class StudentService {

  constructor(private http: HttpClient) {}

    student: Student = new Student();

    getStudentFromARC(arc: string): Observable<any>{

        return this.http.post("http://127.0.0.1:8000/validate_student", {arc_no: arc}).map(data => {
          console.log(data);
          return data;
        });

    }

    updateStudentRegistration (arc: string, reg: number) {
        for (let index = 0; index < Students.length; index++) {
            if(Students[index].arc_number === arc) {
                // return Students[index];
                Students[index].registered = reg;
            }
        }
    }

    freezeSeat(arc: string, freezeFloat: boolean ){
      return this.http.post("http://127.0.0.1:8000/freeze_float_seat", {arc_no: arc, freeze: freezeFloat }).map(data => {
        return data;
      })
    }

    sendPersonalServ(form: NgForm){
      return this.http.post("http://127.0.0.1:8000/send_personal_details",
      {arc_no: form.value.arc_no,
      full_name: form.value.full_name,
      cet_rollno: form.value.cet_rollno,
      email: form.value.email,
      dob: form.value.dob,
      gender: form.value.gender,
      contact: form.value.contact,
      address: form.value.address,
      caste: form.value.caste
    }).map(res => {
      console.log(res);
      return res;
    }, error => {
      console.log(error);
    })


    }

    sendAcademicServ(form: NgForm, arc:string){
      return this.http.post("http://127.0.0.1:8000/send_academic_details", {
        arc_no: arc,
        cet_maths: form.value.cet_maths,
        cet_physics: form.value.cet_physics,
        cet_chemistry: form.value.cet_chemistry,
        cet_rank: form.value.cet_rank,
        cet_total: form.value.cet_total,
        score_twelfth: form.value.score_twelfth,
        score_tenth: form.value.score_tenth
      })
      .map(res => {
        console.log(res);
        return res;
      })
    }

    sendDocsServ(form: NgForm, arc: string){
      return this.http.post("http://127.0.0.1:8000/send_documents", {
      arc_no: arc,
      cet_application: form.value.cet_application,
      twelfth_marksheet: form.value.twelfth_marksheet,
      tenth_marksheet: form.value.tenth_marksheet,
      domicile: form.value.domicile,
      migration_certificate: form.value.migration_certificate
    }).map(res => {
        console.log(res);
        return res;
    });
    }

    sendPrefsServ (form: NgForm, arc: string) {
      return this.http.post("http://127.0.0.1:8000/send_preferences",{
        arc_no: arc,
        first_pref: form.value.first_pref,
        second_pref: form.value.second_pref,
        third_pref: form.value.third_pref,
        fourth_pref: form.value.fourth_pref
      }).map(res => {
        console.log(res);
        return res;
      })
    }






}
