import { Injectable } from "@angular/core";
import { Admin } from "../models/admin.model";
// import { Students } from "../data-mock/student.data";
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import 'rxjs/add/operator/map';


// import { of } from 'rxjs/observable/of';
import 'rxjs/observable/of';
// import { timingSafeEqual } from "crypto";
import { NgForm } from "@angular/forms";

@Injectable()
export class AdminService {
  admin: Admin;
  constructor (private http: HttpClient){

  }

  setAdmin(admin: Admin){
    this.admin = admin;
    console.log(this.admin);
  }

  adminDetails(){
    return this.admin;
  }

  getAdmin(uname: string, pass: string): Observable<any>{
    return this.http.post("http://127.0.0.1:8000/validate_staff", {username: uname, password: pass}).
    map(data => {
      console.log(JSON.parse(data['Students']));
      return data;
    })
    // return {authorization: "success"};
  }


  confirmAdmission(uname: string, pass: string, arc: string) {
    return this.http.post("http://127.0.0.1:8000/confirm_student_admission", {username: uname, password: pass, arc_no: arc}).
    map(data => {
      console.log(data);
      return data;
    })
  }


  allocateSeats(uname:string, pass: string, form: NgForm) {
    return this.http.post("http://127.0.0.1:8000/allocate_seats", {
      username: uname,
      password: pass,
      cs: form.value.cs,
      it: form.value.it,
      extc: form.value.extc,
      etrx: form.value.etrx
    }).map(res => {
      console.log(res);
      return res;
    })
  }

}
