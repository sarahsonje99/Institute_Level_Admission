import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { AdminService } from "../../../shared/admin.service";
import { Admin } from "../../../models/admin.model";

@Component({
  selector: "app-new-round",
  templateUrl: "./new-round.component.html",
  styleUrls: ["./new-round.component.css"]
})
export class NewRoundComponent implements OnInit {
  constructor(private adminserv: AdminService) {}
  admin: Admin;
  round_no: number = 0;

  ngOnInit() {}

  allocationData(form: NgForm) {
    this.admin = { username: "admin", password: "admin@123" };

    this.adminserv.allocateSeats("admin", "admin@123", form).subscribe(res => {
      console.log(res);
      this.round_no += 1;
      alert("Allocation round complete. Confirm Student Admission now");
    });

    // console.log(form);
  }
}
