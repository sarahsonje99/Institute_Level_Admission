import { Component, OnInit } from "@angular/core";
import { AdminService } from "src/app/shared/admin.service";
import { StudentService } from "src/app/shared/student.service";
import { NgForm } from "@angular/forms";
import { Admin } from "src/app/models/admin.model";

@Component({
  selector: "app-view-alloc",
  templateUrl: "./view-alloc.component.html",
  styleUrls: ["./view-alloc.component.css"]
})
export class ViewAllocComponent implements OnInit {
  no_selection: boolean = true;
  confirm: boolean = false;
  run: boolean = false;
  admin: Admin;
  Students: any;
  // finalStudents: {name: string, cet: string, branch: string, rrank: string}[];
  finalStudents: any;
  finalAllocatedStudents: {
    name: string;
    cet: string;
    branch: string;
    rrank: string;
  }[] = [];
  Student_alloc: any;
  showDet: boolean = false;

  allocated_name: string;
  allocated_arc: string;
  allocated_cet_rank: string;
  allocated_round_rank: string;
  allocated_branch: any;

  round_no: number = 0;

  constructor(
    private adminserv: AdminService,
    private studserv: StudentService
  ) {
    this.finalAllocatedStudents.sort((a, b) => {
      if (a.rrank < b.rrank) return -1;
      else if (a.rrank > b.rrank) return 1;
      else return 0;
    });
  }

  ngOnInit() {
    this.adminserv.getAdmin("admin", "admin@123").subscribe(res => {
      // this.Students = res;
      this.Students = JSON.parse(res.Students);
      console.log(this.Students);
      // this.Students.forEach(element => {
      // console.log(element);
      // this.finalStudents.push({
      // name: element.
      // })

      this.finalStudents = Object.keys(this.Students).map(key => {
        return this.Students[key];
      });
      // console.log(this.finalStudents[0]);
      for (var i = 0; i < this.finalStudents.length; i++) {
        var obj = this.finalStudents[i];
        let nam = JSON.parse(obj.Personal).full_name;
        let cetr = JSON.parse(obj.Academic).cet_rank;

        let latestRound = Object.keys(JSON.parse(obj.Allocation)).length;
        let bran = this.changeIdToBranch(
          JSON.parse(obj.Allocation)[latestRound].allocated_branch
        );
        let rank = JSON.parse(obj.Allocation)[latestRound].round_rank;

        this.finalAllocatedStudents.push({
          name: nam,
          cet: cetr,
          branch: bran,
          rrank: rank
        });
        // this.allocated_branch = JSON.parse(obj.Allocation)[latestRound].allocated_branch;
        // this.allocated_branch = this.changeIdToBranch(this.allocated_branch);
        // console.log(bran);
        // console.log(JSON.parse(obj.Allocation));
      }
      console.log(this.finalAllocatedStudents[0]);
    });
  }

  sortAccounts(prop: string) {
    const sorted = this.finalAllocatedStudents.sort((a, b) =>
      a[prop] > b[prop] ? 1 : a[prop] === b[prop] ? 0 : -1
    );
    // asc/desc
    if (prop.charAt(0) === "-") {
      sorted.reverse();
    }
    return sorted;
  }

  rowClicked() {
    this.no_selection = !this.no_selection;
  }

  allocationData(form: NgForm) {
    this.admin = { username: "admin", password: "admin@123" };

    this.adminserv.allocateSeats("admin", "admin@123", form).subscribe(res => {
      console.log(res);
      this.round_no += 1;
      alert("Allocation round complete. Confirm Student Admission now");
    });

    // console.log(form);
  }

  confirmAd(form: NgForm) {
    this.admin = this.adminserv.adminDetails();
    // console.log(this.admin);
    this.adminserv
      .confirmAdmission("admin", "admin@123", form.value.arc_no)
      .subscribe(res => {
        console.log(res);
      });
    this.studserv.getStudentFromARC(form.value.arc_no).subscribe(res => {
      console.log(res);
      this.allocated_arc = form.value.arc_no;
      this.Student_alloc = res;
      console.log(this.Student_alloc);
      this.showDet = true;
      console.log(JSON.parse(this.Student_alloc.Personal).full_name);
      this.showAllocatedStudentDetails(this.Student_alloc);
    });
  }

  showAllocatedStudentDetails(allocatedStudent) {
    this.allocated_name = JSON.parse(allocatedStudent.Personal).full_name;

    let latestRound = Object.keys(JSON.parse(allocatedStudent.Allocation))
      .length;
    this.allocated_branch = JSON.parse(allocatedStudent.Allocation)[
      latestRound
    ].allocated_branch;
    this.allocated_branch = this.changeIdToBranch(this.allocated_branch);

    // console.log(JSON.parse(allocatedStudent.Academic).cet_rank);
    this.allocated_cet_rank = JSON.parse(allocatedStudent.Academic).cet_rank;

    this.allocated_round_rank = JSON.parse(allocatedStudent.Allocation)[
      latestRound
    ].round_rank;
    // console.log(JSON.parse(allocatedStudent.Allocation)[latestRound].round_rank);

    // console.log(this.allocated_branch);
  }

  changeIdToBranch(par) {
    switch (par) {
      case 1:
        return "Computer Science";
      case 2:
        return "Information Technology";
      case 3:
        return "Electronics & Tele-Communication";
      case 4:
        return "Electronics";
    }
  }
}
