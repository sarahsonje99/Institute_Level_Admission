import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewAllocComponent } from './view-alloc.component';

describe('ViewAllocComponent', () => {
  let component: ViewAllocComponent;
  let fixture: ComponentFixture<ViewAllocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewAllocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewAllocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
