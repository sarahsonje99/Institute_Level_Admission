import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmAdComponent } from './confirm-ad.component';

describe('ConfirmAdComponent', () => {
  let component: ConfirmAdComponent;
  let fixture: ComponentFixture<ConfirmAdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmAdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmAdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
