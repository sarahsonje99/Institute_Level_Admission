import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { AdminService } from "../../../shared/admin.service";
import { Admin } from "../../../models/admin.model";
import { StudentService } from "../../../shared/student.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-confirm-ad",
  templateUrl: "./confirm-ad.component.html",
  styleUrls: ["./confirm-ad.component.css"]
})
export class ConfirmAdComponent implements OnInit {
  no_selection: boolean = true;
  confirm: boolean = false;
  run: boolean = false;
  admin: Admin;
  Students: any;
  // finalStudents: {name: string, cet: string, branch: string, rrank: string}[];
  finalStudents: any;
  finalAllocatedStudents: {
    name: string;
    cet: string;
    branch: string;
    rrank: string;
  }[] = [];
  Student_alloc: any;
  showDet: boolean = false;

  allocated_name: string;
  allocated_arc: string;
  allocated_cet_rank: string;
  allocated_round_rank: string;
  allocated_branch: any;

  round_no: number = 0;

  constructor(
    private adminserv: AdminService,
    private studserv: StudentService,
    private router: Router
  ) {}

  ngOnInit() {}

  confirmAd(form: NgForm) {
    this.admin = this.adminserv.adminDetails();
    // console.log(this.admin);
    this.adminserv
      .confirmAdmission("admin", "admin@123", form.value.arc_no)
      .subscribe(res => {
        console.log(res);
      });
    this.studserv.getStudentFromARC(form.value.arc_no).subscribe(res => {
      console.log(res);
      this.allocated_arc = form.value.arc_no;
      this.Student_alloc = res;
      console.log(this.Student_alloc);
      this.showDet = true;
      console.log(JSON.parse(this.Student_alloc.Personal).full_name);
      this.showAllocatedStudentDetails(this.Student_alloc);
    });
  }

  showAllocatedStudentDetails(allocatedStudent) {
    this.allocated_name = JSON.parse(allocatedStudent.Personal).full_name;

    let latestRound = Object.keys(JSON.parse(allocatedStudent.Allocation))
      .length;
    this.allocated_branch = JSON.parse(allocatedStudent.Allocation)[
      latestRound
    ].allocated_branch;
    this.allocated_branch = this.changeIdToBranch(this.allocated_branch);

    // console.log(JSON.parse(allocatedStudent.Academic).cet_rank);
    this.allocated_cet_rank = JSON.parse(allocatedStudent.Academic).cet_rank;

    this.allocated_round_rank = JSON.parse(allocatedStudent.Allocation)[
      latestRound
    ].round_rank;
    // console.log(JSON.parse(allocatedStudent.Allocation)[latestRound].round_rank);

    // console.log(this.allocated_branch);
  }

  changeIdToBranch(par) {
    switch (par) {
      case 1:
        return "Computer Science";
      case 2:
        return "Information Technology";
      case 3:
        return "Electronics & Tele-Communication";
      case 4:
        return "Electronics";
    }
  }
}
