import { Component, OnInit, AfterViewChecked } from "@angular/core";

declare let paypal: any;
@Component({
  selector: "app-payment",
  templateUrl: "./payment.component.html",
  styleUrls: ["./payment.component.css"]
})
export class PaymentComponent implements OnInit, AfterViewChecked {
  addScript: boolean = false;
  finalAmount: number = 5;

  paypalConfig = {
    env: "sandbox",
    client: {
      sandbox:
        "AaZ4rXbUaPQ22y65baehNok7MwDw_Dg63Rk19BrhNMwOB-Y2eOAHVUe9E0dfCSijuueXM5A5aRL3XHwi",
      production: "<prod-key>"
    },
    commit: true,
    payment: (data, actions) => {
      return actions.payment.create({
        payment: {
          transactions: [{ amount: { total: 5, currency: "USD" } }]
        }
      });
    },
    onAuthorize: (data, actions) => {
      return actions.payment.execute().then(payment => {
        //something
      });
    }
  };
  constructor() {}

  ngOnInit() {}

  ngAfterViewChecked(): void {
    if (!this.addScript) {
      this.addPaypalScript().then(() => {
        paypal.Button.render(this.paypalConfig, "#checkout-btn");
      });
    }
  }

  addPaypalScript() {
    this.addScript = true;
    return new Promise((resolve, reject) => {
      let scripttagelement = document.createElement("script");
      scripttagelement.src = "https://www.paypalobjects.com/api/checkout.js";
      scripttagelement.onload = resolve;
      document.body.appendChild(scripttagelement);
    });
  }
}
