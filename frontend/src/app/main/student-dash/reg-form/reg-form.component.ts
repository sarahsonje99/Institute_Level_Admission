import { Component, OnInit, DoCheck, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { StudentService } from '../../../shared/student.service';
import { Student } from '../../../models/student.model';
// import { EventEmitter } from 'events';

@Component({
  selector: 'app-reg-form',
  templateUrl: './reg-form.component.html',
  styleUrls: ['./reg-form.component.css']
})
export class RegFormComponent implements OnInit {

  student: Student;
  reg_page: number = 0;
  x: any;
  arc: string;

  constructor(private router: Router,
    private activeroute: ActivatedRoute,
    private studServ: StudentService) { }

  ngOnInit() {
    this.x = document.getElementsByClassName('page');
    this.x[this.reg_page].style.display = "block";
    this.activeroute.params.subscribe(
    (params: Params) => {
      this.arc = params['s_arc'];
    }
  );
  }


  submit(form: NgForm) {

    console.log(this.x);
    // console.log("page: "+this.reg_page+this.x[this.reg_page].style.display);
    this.x[this.reg_page].style.display="none";
    this.reg_page++;
    this.x[this.reg_page].style.display = "block";
    console.log(form);
  }

  registerStudent() {
    // this.activeroute.params.subscribe((params: Params) => {
      // this.student = this.studServ.getStudentFromARC(params['s_arc']);
      // this.studServ.updateStudentRegistration(this.student.arc_number, ++this.student.registered);
      console.log(this.student);
      this.router.navigate(['main/student_dash', this.arc]);
    // })
  }

  sendPersonal(form: NgForm) {
    console.log(form);
    this.studServ.sendPersonalServ(form).subscribe(res => {
      console.log(res);
      this.x[this.reg_page].style.display="none";
    this.reg_page++;
    this.x[this.reg_page].style.display = "block";
    });

  }

  sendAcademic(form: NgForm) {
    console.log(form);
    this.studServ.sendAcademicServ(form, this.arc).
    subscribe( res => {
      console.log(res);
      this.x[this.reg_page].style.display="none";
    this.reg_page++;
    this.x[this.reg_page].style.display = "block";
    })
  }

  sendDocs(form: NgForm){
    console.log(form);
    if(form.value.cet_application == ""){
      form.value.cet_application = false;
    }
    if(form.value.twelfth_marksheet == ""){
      form.value.twelfth_marksheet = false;
    }
    if(form.value.tenth_marksheet == ""){
      form.value.tenth_marksheet = false;
    }
    if(form.value.domicile == ""){
      form.value.domicile = false;
    }
    if(form.value.migration_certificate == ""){
      form.value.migration_certificate = false;
    }

    console.log(form);

    this.studServ.sendDocsServ(form, this.arc).
    subscribe( res => {
      console.log(res);
      this.x[this.reg_page].style.display="none";
    this.reg_page++;
    this.x[this.reg_page].style.display = "block";
    })
  }

  sendPref(form: NgForm) {
    console.log(form);
    this.studServ.sendPrefsServ(form, this.arc).
    subscribe( res => {
      console.log(res);
      this.x[this.reg_page].style.display="none";
    this.reg_page++;
    this.x[this.reg_page].style.display = "block";
    })
  }

}
