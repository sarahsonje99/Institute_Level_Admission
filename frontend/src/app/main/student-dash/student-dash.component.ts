import { Component, OnInit, OnChanges, DoCheck } from '@angular/core';
import { Student } from '../../models/student.model';
import { StudentService } from '../../shared/student.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { IfStmt } from '@angular/compiler';
import { isEmpty } from 'rxjs/operators';

@Component({
  selector: 'app-student-dash',
  templateUrl: './student-dash.component.html',
  styleUrls: ['./student-dash.component.css']
})
export class StudentDashComponent implements OnInit {

  student: Student;
  stud_arc: string;
  Resp: any;
  round: number;
  allocated_branch_id: number;
  allocated_branch: any;
  admission_confirmed: number;
  no_alloc: boolean = true;


  constructor(private studServ: StudentService,
    private activeRoute: ActivatedRoute,
    private router: Router) {

    // console.log(this.Resp);

    }

  ngOnInit() {
    this.activeRoute.params.subscribe(
      (params: Params) => {
        this.stud_arc = params['s_arc'];
      }
    );

    this.studServ.getStudentFromARC(this.stud_arc).subscribe(res => {
      let alloc_json = JSON.parse(res.Allocation);
      this.Resp = alloc_json;


      // console.log(this.allocated_branch);
      // console.log("show me data "+JSON.stringify(this.Resp));
      //   console.log(alloc_json[1]['round_number']);
      //   this.round = 0;
    // }

      if(res.Allocation === "{}"){
        this.no_alloc = true;
        console.log("sdfg");
      }

      else {
        this.no_alloc = false;
        let latestRound = Object.keys(this.Resp).length;
      this.allocated_branch_id = this.Resp[latestRound].allocated_branch;
      this.allocated_branch = this.idToBranch(this.allocated_branch_id);
      this.round = this.Resp[latestRound].round_number;
      }
      console.log("go here please");

      if(res.rank==0){
        this.round = 0;
      console.log("registered but not filled details");
      // this.studentReg();
      }

      // }
      // else {
      //   this.round = alloc_json[1]['round_number'];
      //   console.log("current branch: "+alloc_json[1]['allocated_branch']);
      //   this.allocated_branch_id = alloc_json[1]['allocated_branch'];
      //   this.allocated_branch = this.idToBranch(this.allocated_branch_id);
      //   if(alloc_json[1]['admission_confirmed'] == true)
      //     this.admission_confirmed = 1;
      //   else this.admission_confirmed = 0;
      // }
    });



    // this.student = this.studServ.getStudentFromARC(this.stud_arc);

  }

  studentReg() {
    this.router.navigate(['registration_form'], { relativeTo: this.activeRoute });
  }

  idToBranch(id: number){
    switch(id){
      case 1: {
        return 'Computer Science';
      }
      case 2: {
        return 'Information Technology';
      }
      case 3: {
        return 'Electronics and TeleCommunication';
      }
      case 4: {
        return 'Electronics';
      }
    }
  }

  freezeFloat(freeze: boolean){
    if(freeze){
      alert("Are you sure you want to freeze this particular seat?")
    }
    else {
      alert("Are you sure you want to float this particular seat?")

    }
    console.log(this.stud_arc);
    this.studServ.freezeSeat(this.stud_arc, freeze).subscribe(dat =>{
      console.log(dat);
    })
  }






}
