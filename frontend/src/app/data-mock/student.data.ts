import { Student } from "../models/student.model";

export const Students: Student[] = [
    { arc_number: 'EN101', cet_number: '1005', registered: 0 },
    { arc_number: 'EN102', cet_number: '1025', registered: 0 },
    { arc_number: 'EN103', cet_number: '1035', registered: 0 },
    { arc_number: 'EN104', cet_number: '1045', registered: 0 },
    { arc_number: 'EN105', cet_number: '1065', registered: 0 },
    { arc_number: 'EN106', cet_number: '1075', registered: 0 },
    { arc_number: 'EN107', cet_number: '1085', registered: 0 },
]

export class StudentData { }