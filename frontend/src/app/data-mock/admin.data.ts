import { Admin } from "../models/admin.model";

export const Admins: Admin[] = [
    {username: 'admin1', password: 'pass'},
    {username: 'admin2', password: 'pass'},
    {username: 'admin3', password: 'pass'},
]